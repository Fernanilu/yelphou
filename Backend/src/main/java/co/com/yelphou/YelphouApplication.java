package co.com.yelphou;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class YelphouApplication {

    public static void main(String[] args) {
        SpringApplication.run(YelphouApplication.class, args);
    }

}
